from setuptools import setup, find_packages

with open('requirements.txt') as f:
	install_requires = f.read().strip().split('\n')

# get version from __version__ variable in federated_venue/__init__.py
from federated_venue import __version__ as version

setup(
	name='federated_venue',
	version=version,
	description='Application for venues federated through the Venues Federation application',
	author='Dokos SAS',
	author_email='hello@dokos.io',
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
