from frappe.custom.doctype.custom_field.custom_field import create_custom_fields


def after_install():
	after_migrate()


def after_migrate():
	create_custom_fields({
		"Customer": [
			{
				"fieldname": "bil_subscribed",
				"label": "Abonné⋅e au Badge Inter-Lieux",
				"fieldtype": "Check",
				"default": "0",
				"insert_after": "customer_group",
				# "options": "No\nYes\nExpired",
			},
		],
	})
