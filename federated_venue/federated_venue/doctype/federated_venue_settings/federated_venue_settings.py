# Copyright (c) 2022, Dokos SAS and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document

class FederatedVenueSettings(Document):
	def onload(self):
		if not self.webhook_secret:
			self.set_onload("webhook_secret", frappe.generate_hash(length=20))
