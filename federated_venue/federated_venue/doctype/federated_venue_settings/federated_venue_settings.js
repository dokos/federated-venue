// Copyright (c) 2022, Dokos SAS and contributors
// For license information, please see license.txt

frappe.ui.form.on('Federated Venue Settings', {
	onload: function(frm) {
		if (!frm.doc.webhook_secret) {
			console.log(frm.doc.__onload.webhook_secret)
			frm.set_value("webhook_secret", frm.doc.__onload.webhook_secret)
		}
	}
});
