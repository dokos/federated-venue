import hashlib
import hmac
import base64

import frappe
from frappe import _
from erpnext.controllers.website_list_for_contact import get_customers_suppliers

@frappe.whitelist()
def user_access_to_federation(*args, **kwargs):
	# For all customers of the user, check if the value `bil_subscribed` is `Yes`.
	# If it is, return `True`. Otherwise, return `False`.
	customers, _ = get_customers_suppliers("Customer", frappe.session.user)
	for customer in (customers or []):
		bil_subscribed = frappe.db.get_value("Customer", customer, "bil_subscribed")
		if bil_subscribed in ("Yes", True, 1):
			return True
	return False

@frappe.whitelist(allow_guest=True)
def checkin_callback(*args, **kwargs):
	verify_signature()

	doc = frappe.new_doc("Venue Checkin")
	doc.update(
		frappe.parse_json(
			frappe.safe_decode(frappe.request.data)
		)
	)
	doc.insert(ignore_permissions=True, ignore_if_duplicate=True)
	doc.submit()

def verify_signature():
	dig = base64.b64encode(
		hmac.new(
			bytes(frappe.db.get_single_value("Federated Venue Settings", "webhook_secret").encode()),
			frappe.request.data,
			hashlib.sha256
		).digest()
	)

	signature = frappe.local.request.headers.get('X-Frappe-Webhook-Signature')

	if dig != bytes(signature.encode()):
		frappe.throw(_('Webhook Signature Verification Failed'), exc=frappe.PermissionError)