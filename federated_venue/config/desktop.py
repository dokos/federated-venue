from frappe import _

def get_data():
	return [
		{
			"module_name": "Federated Venue",
			"color": "grey",
			"icon": "octicon octicon-file-directory",
			"type": "module",
			"label": _("Federated Venue")
		}
	]
